// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UnrealEnttHeader.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "Containers/Ticker.h"
#include "UnrealEnttSubsystem.generated.h"

/**
 * 
 */
UCLASS()
class UNREALENTT_API UUnrealEnttSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()

protected:
	TUniquePtr<UnrealEntt::FEnttWorld> World;
	FTickerDelegate OnTickDelegate;
	FDelegateHandle OnTickHandle;

public:
	virtual void Initialize(FSubsystemCollectionBase &Collection) override;
	virtual void Deinitialize() override;
	bool Tick(float DeltaTime) const;

public:
	UnrealEntt::FEnttWorld *GetEnttWorld() const;
};
