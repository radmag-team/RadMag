﻿using UnrealBuildTool;

public class GameModule : ModuleRules
{
	public GameModule(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.NoSharedPCHs;
		PrivatePCHHeaderFile = "PCHBasic.h";
		CppStandard = CppStandardVersion.Cpp17;

		PublicDependencyModuleNames.AddRange(new string[] {
			"Core",
			"CoreUObject",
			"Engine",
			"UnrealEntt",
			"UnrealFastNoiseLite",
			"ProceduralMeshComponent",
		});
		PublicIncludePaths.AddRange(new string[] {"GameModule/Public"});
		PrivateIncludePaths.AddRange(new string[] {"GameModule/Private"});
	}
}