// Fill out your copyright notice in the Description page of Project Settings.

#include "Actors/MapActor.h"

AMapActor::AMapActor()
{
	PrimaryActorTick.bCanEverTick = false;
	MeshComponent = CreateDefaultSubobject<UProceduralMeshComponent>(FName(TEXT("MeshComponent")));
	MeshMaterial = nullptr;
}

void AMapActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AMapActor::BeginPlay()
{
	Super::BeginPlay();

	if(MeshMaterial)
	{
		MeshComponent->SetMaterial(0, MeshMaterial);
	}
}

void AMapActor::Update(const TArray<FVector>& Vertices, const TArray<int32>& Triangles, const TArray<FLinearColor>& VertexColors)
{
	MeshComponent->CreateMeshSection_LinearColor(0, Vertices, Triangles, TArray<FVector>(), TArray<FVector2D>(),
                            VertexColors, TArray<FProcMeshTangent>(), false);
}
