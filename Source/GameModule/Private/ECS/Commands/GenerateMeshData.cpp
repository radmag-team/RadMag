// Fill out your copyright notice in the Description page of Project Settings.

#include "ECS/Commands/GenerateMeshData.h"
#include "ECS/ECSHeader.h"

namespace GameModule
{
    ESystemStatus FGenerateMeshData::Tick(FEnttData &Data, float DeltaTime)
    {
        auto &MapData = Data.GetResource<FMapData>();
        auto HexVi = std::as_const(Data).GetView(entt::type_list<CHexTag, CHexData>{});
        HexVi.each([&MapData](auto Entity, const auto &HexData)
                   {
                       const auto OffsetCoordinate = FMapData::ConvertToOffsetCoordinate(HexData.CubeCoordinate);
                       const FVector Center = MapData.ConvertToRealCoordinate(OffsetCoordinate);
                       const auto Color = MapData.GetHexColor(HexData.HeightValue);

                       SIZE_T VertexIndex = MapData.Vertices.Num();
                       MapData.Vertices.Add(Center + MapData.Corners[0]);
                       MapData.Vertices.Add(Center + MapData.Corners[1]);
                       MapData.Vertices.Add(Center + MapData.Corners[2]);
                       MapData.Vertices.Add(Center + MapData.Corners[3]);
                       MapData.Vertices.Add(Center + MapData.Corners[4]);
                       MapData.Vertices.Add(Center + MapData.Corners[5]);
                       for (SIZE_T Index = 1; Index < 5; ++Index)
                       {
                           MapData.Triangles.Add(VertexIndex);
                           MapData.Triangles.Add(VertexIndex + Index);
                           MapData.Triangles.Add(VertexIndex + Index + 1);
                       }
                       for (SIZE_T Index = 0; Index < 6; ++Index)
                           MapData.VertexColors.Add(Color);
                   });

        return ESystemStatus::Stop;
    };
}