// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Components.h"

class AMapActor;

namespace GameModule
{
    struct FMapData
    {
        int32 MapLength;
        int32 MapWidth;
        float DeepWater;
        float ShallowWater;
        float Sand;
        float Grass;
        float Forest;
        float Rock;
        float Snow;
        TArray<UnrealEntt::EEntity> Hexes;

        float OuterRadius = 10.f;
        float OuterToInner = 0.866025404f;
        float InnerRadius = OuterRadius * OuterToInner;
        TArray<FVector> Corners{
            FVector(0.f, OuterRadius, 0.f),
            FVector(InnerRadius, 0.5f * OuterRadius, 0.f),
            FVector(InnerRadius, -0.5f * OuterRadius, 0.f),
            FVector(0.f, -OuterRadius, 0.f),
            FVector(-InnerRadius, -0.5f * OuterRadius, 0.f),
            FVector(-InnerRadius, 0.5f * OuterRadius, 0.f),
            FVector(0.f, OuterRadius, 0.f)};

        TArray<FVector> Vertices;
        TArray<int32> Triangles;
        TArray<FLinearColor> VertexColors;
        AMapActor *MapActor;

    public:
        auto GetInnerDiameter() const -> float
        {
            return 2.f * OuterRadius * OuterToInner;
        }

        decltype(auto) ConvertToRealCoordinate(const FIntVector2 &OffsetCoordinate) const
        {
            const auto X = (OffsetCoordinate.X + OffsetCoordinate.Y * 0.5f - OffsetCoordinate.Y / 2) *
                           GetInnerDiameter();
            const auto Y = OffsetCoordinate.Y * (OuterRadius * 1.5f);
            const auto Z = 0.f;
            return FVector(X, Y, Z);
        }

        static FORCEINLINE FIntVector ConvertToCubeCoordinate(const FIntVector2 &OffsetCoordinate)
        {
            const auto X = OffsetCoordinate.X - (OffsetCoordinate.Y - (OffsetCoordinate.Y & 1)) / 2;
            const auto Z = OffsetCoordinate.Y;
            const auto Y = -X - Z;
            return FIntVector(X, Y, Z);
        }

        static FORCEINLINE FIntVector2 ConvertToOffsetCoordinate(const FIntVector &CubeCoordinate)
        {
            const auto Col = CubeCoordinate.X + (CubeCoordinate.Z - (CubeCoordinate.Z & 1)) / 2;
            const auto Row = CubeCoordinate.Z;
            return FIntVector2(Col, Row);
        }

        FLinearColor GetHexColor(float Height)
        {
            if (Height < 0)
                return FLinearColor::Black;

            if (Height < DeepWater)
                return FLinearColor(0, 0, 0.5f, 1);

            if (Height < ShallowWater)
                return FLinearColor(25 / 255.f, 25 / 255.f, 150 / 255.f, 1);

            if (Height < Sand)
                return FLinearColor(240 / 255.f, 240 / 255.f, 64 / 255.f, 1);

            if (Height < Grass)
                return FLinearColor(50 / 255.f, 220 / 255.f, 20 / 255.f, 1);

            if (Height < Forest)
                return FLinearColor(16 / 255.f, 160 / 255.f, 0, 1);

            if (Height < Rock)
                return FLinearColor(0.5f, 0.5f, 0.5f, 1);

            return FLinearColor::White;
        }
    };
}