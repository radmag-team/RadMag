// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "UnrealEnttHeader.h"
#include "Components.h"

namespace GameModule
{
	using CHexTag = entt::tag<entt::hashed_string("Hex")>;
	using EHex = entt::type_list<CHexTag, CHexData>::type;

}
