// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MapDataInput.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct FMapDataInput
{
    GENERATED_BODY()

public:

	UPROPERTY(EditInstanceOnly, Category = "Map config")
	int32 MapLength;

	UPROPERTY(EditInstanceOnly, Category = "Map config")
	int32 MapWidth;

    UPROPERTY(EditInstanceOnly, Category = "Map config")
    int Octaves;

	UPROPERTY(EditInstanceOnly, Category = "Map config")
    double Frequency;

    UPROPERTY(EditInstanceOnly, Category = "Map config")
    int Seed;

	UPROPERTY(EditInstanceOnly, Category = "Map config")
	float DeepWater = 0.2f;
	
	UPROPERTY(EditInstanceOnly, Category = "Map config")
	float ShallowWater = 0.4f;

	UPROPERTY(EditInstanceOnly, Category = "Map config")
	float Sand = 0.5f;
	
	UPROPERTY(EditInstanceOnly, Category = "Map config")
	float Grass = 0.7f;
	
	UPROPERTY(EditInstanceOnly, Category = "Map config")
	float Forest = 0.8f;
	
	UPROPERTY(EditInstanceOnly, Category = "Map config")
	float Rock = 0.9f;

	UPROPERTY(EditInstanceOnly, Category = "Map config")
	float Snow = 1;
};