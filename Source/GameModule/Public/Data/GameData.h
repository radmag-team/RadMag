// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "Actors/MapActor.h"
#include "MapDataInput.h"
#include "GameData.generated.h"

/**
 * 
 */
UCLASS()
class GAMEMODULE_API UGameData : public UDataAsset
{
	GENERATED_BODY()

public:

	UPROPERTY(EditInstanceOnly)
	FMapDataInput MapDataInput;

	UPROPERTY(EditInstanceOnly)
	TSubclassOf<AMapActor> MapActorClass;
};
