﻿using UnrealBuildTool;

public class EditorModule : ModuleRules
{
	public EditorModule(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.NoSharedPCHs;
		PrivatePCHHeaderFile = "PCHBasic.h";
		CppStandard = CppStandardVersion.Cpp17;

		PublicDependencyModuleNames.AddRange(new string[] {
			"Core",
			"CoreUObject",
			"Engine",
			"UnrealEntt",
			"GameModule",
		});
		PublicIncludePaths.AddRange(new string[] {"EditorModule/Public"});
		PrivateIncludePaths.AddRange(new string[] {"EditorModule/Private"});
	}
}