﻿#include "EditorModule/Public/EditorModule.h"
#include "Modules/ModuleManager.h"
#include "Modules/ModuleInterface.h"

IMPLEMENT_GAME_MODULE(FEditorModule, EditorModule);

DEFINE_LOG_CATEGORY(EditorModule);
 
#define LOCTEXT_NAMESPACE "EditorModule"
 
void FEditorModule::StartupModule()
{
	UE_LOG(EditorModule, Warning, TEXT("Editor module has started!"));
}
 
void FEditorModule::ShutdownModule()
{
	UE_LOG(EditorModule, Warning, TEXT("Editor module has shut down"));
}
 
#undef LOCTEXT_NAMESPACE