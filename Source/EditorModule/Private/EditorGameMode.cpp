// Fill out your copyright notice in the Description page of Project Settings.

#include "EditorGameMode.h"
#include "UnrealEnttSubsystem.h"
#include "UnrealEnttHeader.h"
#include "ECS/ECSHeader.h"
#include "ECS/CommandsList.h"
#include "Engine/GameInstance.h"
#include "Data/GameData.h"
#include "Engine/World.h"

void AEditorGameMode::BeginPlay()
{
	Super::BeginPlay();

	const auto GameInstance = GetGameInstance();
	auto EnttWorld = GameInstance->GetSubsystem<UUnrealEnttSubsystem>()->GetEnttWorld();

	
	EnttWorld->AddCommands(MakeUnique<GameModule::FGenerateMapData>(GameData->MapDataInput));	
	EnttWorld->AddCommands(MakeUnique<GameModule::FGenerateMeshData>());	
	EnttWorld->AddCommands(MakeUnique<GameModule::FCreateMapActor>(GetWorld(), GameData->MapActorClass));
}