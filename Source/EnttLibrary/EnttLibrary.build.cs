﻿using System.IO;
using UnrealBuildTool;

public class EnttLibrary : ModuleRules
{
	public EnttLibrary(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.NoSharedPCHs;
		PrivatePCHHeaderFile = "PCHBasic.h";
		CppStandard = CppStandardVersion.Cpp17;
		PublicIncludePaths.Add(Path.Combine(ModuleDirectory, "entt"));
	}
}