﻿using System.IO;
using UnrealBuildTool;

public class UnrealFastNoiseLite : ModuleRules
{
	public UnrealFastNoiseLite(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.NoSharedPCHs;
		PrivatePCHHeaderFile = "PCHBasic.h";
		CppStandard = CppStandardVersion.Cpp17;
		PublicIncludePaths.Add(Path.Combine(ModuleDirectory, "Public/FastNoiseLite"));
		PublicDependencyModuleNames.AddRange(new string[]
		{
			"Core"
		});
	}
}